

if False:
    import numpy as np
    import cv2
     
    im = cv2.imread('img/Screenshot_2017-02-01_14-45-47.png')
    imgray = cv2.cvtColor(im,cv2.COLOR_BGR2GRAY)
    ret,thresh = cv2.threshold(imgray,127,255,0)
    contours, hierarchy = cv2.findContours(thresh,cv2.RETR_TREE,cv2.CHAIN_APPROX_SIMPLE)
    cnt = contours[0]

    cv2.drawContours(im,contours,-1,(0,255,0),3)
    cv2.drawContours(im,contours,-1,(0,255,0),-1)
    cv2.drawContours(im,[cnt],0,(255,0,0),-1)

    moments = cv2.moments(cnt)
    area = cv2.contourArea(cnt)
    perimeter = cv2.arcLength(cnt,True)
    approx = cv2.approxPolyDP(cnt,0.1*cv2.arcLength(cnt,True),True)

    print(approx)

    #~ cv2.imshow('img',im)
    #~ cv2.waitKey(0)
    #~ cv2.destroyAllWindows()


if False:
    import cv2
    import numpy as np

    img = cv2.pyrDown(cv2.imread("img/Screenshot_2017-02-01_14-45-47.png", cv2.IMREAD_UNCHANGED))

    ret, thresh = cv2.threshold(cv2.cvtColor(img.copy(), cv2.COLOR_BGR2GRAY) , 127, 255, cv2.THRESH_BINARY)
    image, contours, hier = cv2.findContours(thresh, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)

    for c in contours:
        # find bounding box coordinates
        x,y,w,h = cv2.boundingRect(c)
        cv2.rectangle(img, (x,y), (x+w, y+h), (0, 255, 0), 2)

        # find minimum area
        rect = cv2.minAreaRect(c)
        # calculate coordinates of the minimum area rectangle
        box = cv2.boxPoints(rect)
        # normalize coordinates to integers
        box = np.int0(box)
        # draw contours
        cv2.drawContours(img, [box], 0, (0,0, 255), 3)
        # calculate center and radius of minimum enclosing circle
        (x,y),radius = cv2.minEnclosingCircle(c)
        # cast to integers
        center = (int(x),int(y))
        radius = int(radius)
        # draw the circle
        img = cv2.circle(img,center,radius,(0,255,0),2)

        cv2.drawContours(img, contours, -1, (255, 0, 0), 1)
        cv2.imshow("contours", img)

if False:
    import numpy as np
    import cv2

    img = cv2.imread('img/Screenshot_2017-02-01_14-45-47.png')
    gimg = cv2.imread('img/Screenshot_2017-02-01_14-45-47.png',0)

    ret,thresh = cv2.threshold(gimg,127,255,1)

    contours,h = cv2.findContours(thresh,1,2)

    for cnt in contours:
        approx = cv2.approxPolyDP(cnt,0.01*cv2.arcLength(cnt,True),True)
        print len(approx)
        if len(approx)==5:
            print "pentagon"
            cv2.drawContours(img,[cnt],0,255,-1)
        elif len(approx)==3:
            print "triangle"
            cv2.drawContours(img,[cnt],0,(0,255,0),-1)
        elif len(approx)==4:
            print "square"
            cv2.drawContours(img,[cnt],0,(0,0,255),-1)
        elif len(approx) == 9:
            print "half-circle"
            cv2.drawContours(img,[cnt],0,(255,255,0),-1)
        elif len(approx) > 15:
            print "circle"
            cv2.drawContours(img,[cnt],0,(0,255,255),-1)

    cv2.imshow('img',gimg)
    cv2.waitKey(0)
    cv2.destroyAllWindows()
