import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

#~ get data
#~ np.random.seed(0)
#~ x = np.random.uniform(0.0,1680.0,100)
#~ y = np.random.uniform(0.0,1050.0,100)



dfa = pd.read_csv('csv/31-porxy-scroll-train-frame.csv', sep='\t')
#~ print(dfa.l_por_x.head(), dfa.shape)
#~ trim away first data until trial start
#~ get scroll, subtract scroll constant
#~ scroll compensation, subtract constant top offset
dfa['comp'] = dfa.lpory + dfa.scroll
x = dfa.lporx
y = dfa.comp

#~ img = plt.imread('tmp/00914.png')
#~ img = plt.imread('img/allsylt2.png')
img = plt.imread('img/aois.png')
#~ fig, ax = plt.subplots()
#~ ax.imshow(img)

#~ x = range(300)
#~ ax.imshow(img, extent=[0, 400, 0, 300])
#~ ax.plot(x, x, '--', linewidth=5, color='firebrick')

#display image
#~ plt.figure(1)
#~ plt.imshow(img)
#~ plt.title('image')
#~ plt.show()

#~ 
plt.scatter(x,y,s=0.5, zorder=1)
plt.imshow(img, zorder=0)
plt.show()



