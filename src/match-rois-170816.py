import numpy as np
import cv2
#~ from skimage.measure import structural_similarity as ssim
#~ import math
from matplotlib import pyplot as plt
#~ import pytesseract
#~ from PIL import Image
#~ import re
import csv
#~ from collections import Counter
import os
import argparse
import subprocess
import glob
import random


#~ ##################################################################### visualization
def draw_match(fnum, image, match):
    for m in match:
        m = m[:-4]
        l = m.split(',')
        c = map(int, l[1].split('-')[1:])
        cv2.rectangle(image, (c[0],c[1]), (c[2],c[3]), (0,255,0), 2)
        cv2.putText(image, l[0], (c[0]+10,c[1]+30), cv2.FONT_HERSHEY_SIMPLEX, 0.6, (200,200,0), 2)
    return image

#~ ##################################################################### template matching
def match_texts(fnum, image, texts):
    gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
    for i,t in enumerate(texts):
        (h,w) = t.shape[:2]
        temp = cv2.cvtColor(t, cv2.COLOR_BGR2GRAY)
        result = cv2.matchTemplate(gray, temp, cv2.TM_CCOEFF)
        min_val,max_val,min_loc,max_loc = cv2.minMaxLoc(result)
        xy = max_loc
        wh = (xy[0]+w, xy[1]+h)
        cv2.rectangle(image, xy, wh, (0,0,255), 2)
        #~ draw aoi
        y1 = 62 if xy[1]-200<=0 else xy[1]-200
        y2 = 1005 if xy[1]+200>=1005 else xy[1]+200
        cv2.rectangle(image, (xy[0],y1), (xy[0]+w,y2), (0,255,0), 2)

    return image

#~ ##################################################################### analyses
def analyze_video():
    cap = cv2.VideoCapture('vid/31-e2ec8c29-bgscrrec.mkv')

    fnum = 0
    while(cap.isOpened()):
        #~ current_frame_flag = cv2.cv.CV_CAP_PROP_POS_FRAMES
        #~ total_frames_flag = cv2.cv.CV_CAP_PROP_FRAME_COUNT
        fnum += 1
        ret, frame = cap.read()
        image = frame.copy()

        #~ image = analyze_frame(fnum, image)
        image = analyze_match(fnum, image)

        #~ new ###################
        key = cv2.waitKey(100) & 0xff
        if not ret:
            break
        if key == ord('p'):
            while True:
                key2 = cv2.waitKey(1) or 0xff
                cv2.imshow('frame', image)
                if key2 == ord('p'):
                    break
        cv2.imshow('frame', image)
        if key == 27:
            #~ print(fnum)
            break

        #~ old ###################
        #~ cv2.imshow('frame', image)
        #~ if cv2.waitKey(100) & 0xFF == ord('q'):
            #~ s = 'tmp/' + "%05d" % np.random.randint(1000) + '.png' 
            #~ cv2.imwrite(s, frame)
            #~ break

    cap.release()
    cv2.destroyAllWindows()

def analyze_frame(fnum=0, frame=None, testing=False):
    if frame is None:
        testing = True
        #~ frame = cv2.imread('img/00187.png')
        #~ frame = cv2.imread('tmp/00839.png')
        #~ frame = cv2.imread('tmp/00376.png')
        #~ frame = cv2.imread('tmp/00765.png')
        #~ frame = cv2.imread('tmp/00914.png')
        frame = cv2.imread('tmp/00376.png')
    
    #~ prepare image
    #~ some = draw_lines(frame)
    #~ gray = cv2.cvtColor(some, cv2.COLOR_BGR2GRAY)
    #~ gray = cv2.GaussianBlur(gray, (3,3), 0)
    
    #~ find product aois
    #~ ret, image = cv2.threshold(gray, 245, 255, cv2.THRESH_BINARY)
    #~ image = find_edges(image)
    #~ boxes = find_contours(image)
    
    #~ find texts and chars
    gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
    _,thresh = cv2.threshold(gray, 150,255, cv2.THRESH_BINARY_INV)
    texts = find_texts(thresh)
    #~ chars = find_chars(gray, texts)
    
    #~ draw results
    #~ image,bdata = draw_objects(fnum, frame, boxes, texts, chars)
    #~ image = draw_texts(fnum, frame, texts)
    
    #~ template matching
    #~ texts = get_texts(fnum, frame, texts)
    #~ image = get_texts(fnum, frame, texts)
    #~ image = match_texts(fnum, frame, texts)

    #~ draw aois
    image = analyze_match(fnum, frame)

    #~ handle output, list of lists
    #~ with open("output.csv", "wb") as f:
    #~ with open('tmp/output.csv', 'a') as f:
        #~ writer = csv.writer(f)
        #~ writer.writerows(bdata)
    #~ print(bdata)

    if testing:
        #~ cv2.imwrite('img/test.png', image)
        cv2.imshow('frame', image)
        cv2.waitKey(0)
        #~ plt.imshow(image),plt.show()
    else:
        #~ 
        return image

def analyze_temps():
    cmds = 'rm tmp/matches.csv'
    process = subprocess.Popen(cmds.split(), stdout=subprocess.PIPE)
    output, error = process.communicate()
    imgl = sorted(glob.glob('img/temp/*.png'))
    #~ imgl = glob.glob('img/temp/*.png')
    md = dict({})
    #~ image, gray ###################
    #~ i = cv2.imread('img/temp/00038-491-316-669-716.png', 0)
    #~ g = cv2.cvtColor(i, cv2.COLOR_BGR2GRAY)
    for j,img in enumerate(imgl):
        #~ img = imgl.pop(0)
        #~ imgl.remove(img)
        g = cv2.imread(img,0)
        #~ add border ###################
        row,col= g.shape[:2]
        bottom = g[row-2:row, 0:col]
        mean = cv2.mean(bottom)[0]
        bordersize = 100
        #~ imgb
        b = cv2.copyMakeBorder(g, 
        top=bordersize, 
        bottom=bordersize, 
        left=bordersize, 
        right=bordersize, 
        borderType=cv2.BORDER_CONSTANT, 
        value=[mean,mean,mean])
        #~ match images ###################
        print(len(imgl))
        md[img] = [['start',"%05d" % (j+1)]]
        tl = list(imgl)
        for i,p in enumerate(imgl):
            #~ t = cv2.imread('img/temp/00039-491-316-669-716.png', 0)
            t = cv2.imread(p, 0)
            #~ compare images, get structural similarity index
            #~ s = ssim(g,t)
            #~ methods = [
            #~ 'cv2.TM_CCOEFF', 
            #~ 'cv2.TM_CCOEFF_NORMED', 
            #~ 'cv2.TM_CCORR',
            #~ 'cv2.TM_CCORR_NORMED', 
            #~ 'cv2.TM_SQDIFF', 
            #~ 'cv2.TM_SQDIFF_NORMED']
            res = cv2.matchTemplate(b, t, cv2.TM_CCOEFF_NORMED)
            #~ for m in methods:
            #~ method = eval(m)
            #~ res = cv2.matchTemplate(g, t, method)
            #~ min_val, max_val, min_loc, max_loc = cv2.minMaxLoc(res)
            threshold = 0.9
            loc = np.where(res >= threshold)
            #~ print(loc[0].any(), i, j, p, len(imgl))
            if loc[0].any():
                p = imgl.pop(i)
                #~ x = imgl.pop()
                md[img].append(["%05d" % (j+1), p])
            #~ end temp match
        print(len(md[img]), len(imgl))
        #~ handle output, list of lists
        #~ with open('tmp/matches.csv', 'wb') as f:
        with open('tmp/matches.csv', 'a') as f:
            writer = csv.writer(f)
            writer.writerows(md[img])
    #~ cv2.imshow('temp', b)
    #~ cv2.waitKey(0)

def analyze_match(fnum, image):
    fnum = "%05d" % fnum
    #~ match = subprocess.check_output(['egrep', 'temp/'+fnum, 'csv/matches-170809.csv'])
    #~ match = subprocess.getoutput(['egrep', 'temp/'+fnum, 'csv/matches-170809.csv'])
    try:
        #~ data = subprocess.check_output(func, shell=True)
        match = subprocess.check_output(['egrep', 'temp/'+fnum, 'tmp/matches.csv'])
        match = match.strip().split('\r\n')
        image = draw_match(fnum, image, match)
    except Exception:
        #~ data = None
        #~ continue
        pass
    return image

def analyze_output():
    frame = cv2.imread('img/00839.png')
    some = draw_lines(frame)
    gray = cv2.cvtColor(some, cv2.COLOR_BGR2GRAY)
    cv2.imshow('frame', gray)
    cv2.waitKey(0)

#~ ##################################################################### main
def main():
    pass

if __name__ == '__main__':
    ap = argparse.ArgumentParser()
    ap.add_argument('-a', '--action', help='select action')
    args = vars(ap.parse_args())

    #~ analyze_frame()
    if args['action'] == 'video':
        analyze_video()
    elif args['action'] == 'temps':
        analyze_temps()
    else:
        exit('please provide a valid argument')
    
    #~ analyze_output()









