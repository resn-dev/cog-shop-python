import numpy as np
import cv2
import pandas as pd
import matplotlib.pyplot as plt
import subprocess
import argparse
#~ 
import sys
sys.path.insert(0, './src/pySaliencyMap')
import pySaliencyMap as psal


#~ ##################################################################### saliency
def get_sal(img):
    # initialize
    imgsize = img.shape
    img_width  = imgsize[1]
    img_height = imgsize[0]
    sm = psal.pySaliencyMap(img_width, img_height)
    # computation
    saliency_map = sm.SMGetSM(img)
    binarized_map = sm.SMGetBinarizedSM(img)
    salient_region = sm.SMGetSalientRegion(img)
    # visualize
#    plt.subplot(2,2,1), plt.imshow(img, 'gray')
    plt.subplot(2,2,1), plt.imshow(cv2.cvtColor(img, cv2.COLOR_BGR2RGB))
    plt.title('Input image')
#    cv2.imshow("input",  img)
    plt.subplot(2,2,2), plt.imshow(saliency_map, 'gray')
    plt.title('Saliency map')
#    cv2.imshow("output", map)
    plt.subplot(2,2,3), plt.imshow(binarized_map)
    plt.title('Binarilized saliency map')
#    cv2.imshow("Binarized", binarized_map)
    plt.subplot(2,2,4), plt.imshow(cv2.cvtColor(salient_region, cv2.COLOR_BGR2RGB))
    plt.title('Salient region')
#    cv2.imshow("Segmented", segmented_map)
    #~ 
    #~ plt.show()
    return saliency_map
    
def img_border(imgm):
    #~ add border ###################
    g = imgm
    row,col= g.shape[:2]
    bottom = g[row-2:row, 0:col]
    mean = cv2.mean(bottom)[0]
    bordersize = 100
    #~ imgb
    b = cv2.copyMakeBorder(g, 
    top=bordersize, 
    bottom=bordersize, 
    left=bordersize, 
    right=bordersize, 
    borderType=cv2.BORDER_CONSTANT, 
    value=[mean,mean,mean])
    # ~ value=[255,255,255])
    imgm = b
    #~ show results
    if False:
        # ~ cv2.imshow('border', resize_frame(imgm))
        cv2.imshow('border', imgm)
        cv2.waitKey(0)
    # ~ 
    return imgm

def resize_frame(frame, h=1000):
    #~ h = max height pixels 
    r = float(h) / frame.shape[0]
    dim = (int(frame.shape[1] * r), h)
    #~ 
    rimg = cv2.resize(frame, dim, interpolation=cv2.INTER_AREA)
    #~ 
    #~ print(frame.shape)
    return rimg

#~ ##################################################################### main
def main():
    pass

if __name__ == '__main__':
    ap = argparse.ArgumentParser()
    ap.add_argument('-i', '--image', help='select image')
    args = vars(ap.parse_args())
    imgn = args['image']#bc,bp,by... cc,cp,cy

    # saliency alternative 6, export for matlab analysis
    aoil = pd.read_csv('tmp/aoi-defs-'+imgn+'.csv', sep='\t')
    imgw = cv2.imread('img/study-'+imgn[0]+'/wpage/'+imgn[1]+'.png')
    # iterate over aois
    if True:
        # wpage dims
        (wh,ww) = imgw.shape[:2]
        # context dims
        (ch,cw,cx) = [1050,1680,0]
        # for i,aoi in enumerate(aoil):#aoi_id, aoi_pos
        for index, row in aoil.iterrows():
            # anum = "%03d" % (i+1)
            # x,y,w,h = aoi
            x,y,w,h = map(int, row.aoi_position_xywh.split('|'))
            # study b:
            if y < 200:
                cy = 0
                ay = y
            elif y+h+150 > wh:
                cy = wh-ch
                ay = 1050-(wh-y)
            else:
                cy = y-250
                ay = 250
            # context image
            imgc = imgw[cy:cy+ch, cx:cw]
            # context saliency
            # maps = get_sal(imgc)
            # measure aoi saliency
            # mapa = maps[ay:ay+h, x:x+w]
            # 
            # write context image
            cv2.imwrite('/tmp/cog/matlab/'+row["product"]+'.png', imgc)
            # write aoi position in frame context
            print(row["product"], ay, h, x, w)# should be xywh

    # saliency alternative 5, study a, product image
    if False:
        imgp = cv2.imread("img/study-a/"+imgn+"-o.jpg")
        # saliency analysis, gray for testing
        simg = get_sal(imgp)
        # simg = cv2.cvtColor(imgp, cv2.COLOR_BGR2GRAY)
        timg = cv2.cvtColor(simg, cv2.COLOR_GRAY2RGB)
        # aoi coords
        cmds = "egrep "+imgn+" csv/study-a/aoi-coords.tsv"
        process = subprocess.Popen(cmds.split(), stdout=subprocess.PIPE)
        output, error = process.communicate()
        for s in output.split('\r\n')[:-1]:
            l = s.split('\t')
            anum = l[2]
            if bool(l[4]) and bool(l[6]):
                (x,y) = map(int, l[4:6])
                # (x2,y2) = map(int, l[6:8])
                (w,h) = map(int, l[-2:])
                # measure saliency in aois
                aimg = simg[y:y+h, x:x+w]
                print(anum, l[4], l[5], np.mean(aimg), np.sum(aimg))
                #~ visualize
                cv2.rectangle(timg, (x,y), (x+w,y+h), (0,255,0), 2)
                cv2.putText(timg, str(anum[4:]), (x+10,y+60), cv2.FONT_HERSHEY_SIMPLEX, 1.0, (0,255,0), 4)
                cv2.putText(timg, str(round(np.mean(aimg),2)), (x+10,y+120), cv2.FONT_HERSHEY_SIMPLEX, 1.0, (0,255,0), 2)
                # normalized matlab and python values
                # cmds = "egrep "+anum+" csv/study-a/comp-saliency.csv"
                # process = subprocess.Popen(cmds.split(), stdout=subprocess.PIPE)
                # out2, err2 = process.communicate()
                # a = out2.split('\t')
                # cv2.putText(timg, str(round(float(a[1].strip()),2))+' '+str(round(float(a[2].strip()),2)), (x+10,y+120), cv2.FONT_HERSHEY_SIMPLEX, 1.0, (0,0,255), 2)
            else:
                print(anum, "", "", "", "")
        # show results, write saliency analysis
        # cv2.imshow('img', timg)
        # cv2.waitKey(0)
        timg = timg*255
        cv2.imwrite('img/study-a/'+imgn+'-s.png', timg)

    # saliency alternative 1, product aoi
    if False:
        imgp = '/tmp/cog/cog-shop/'+imgn
        imgm = cv2.imread(imgp)
        (h,w) = imgm.shape[:2]

        imgm = img_border(imgm)
        smap = get_sal(imgm)
        simg = smap[100:100+h, 100:100+w]
        cv2.imshow('simg', simg)
        cv2.waitKey(0)

        print(imgn, np.mean(smap), np.sum(smap))

    # saliency alternative 2, frame
    if False:
        # c.png|481|182|221|402
        # c.png\|961\|2785\|221\|375
        # c.png\|962\|4916\|220\|432
        aois = args['image']
        aoil = aois.split('|')
        x,y,w,h = map(int, aoil[1:-1])
        # web image
        wimg = cv2.imread('/tmp/cog-shop/'+aoil[0])
        (wh,ww) = wimg.shape[:2]
        # context dims
        (ch,cw,cx) = [1050,1680,0]
        if y < 200:
            cy = 0
            ay = y
        elif y+h+150 > wh:
            cy = wh-ch
            ay = 1050-(wh-y)
        else:
            cy = y-250
            ay = 250
        # context image
        cimg = wimg[cy:cy+ch, cx:cw]
        # context saliency
        smap = get_sal(cimg)
        # measure aoi saliency
        simg = smap[ay:ay+h, x:x+w]
        # cv2.imshow('simg', simg)
        # cv2.waitKey(0)
        # simg = smap*255
        # cv2.imwrite('tmp/sa2.png', simg)
        # 
        print(aoil[-1], np.mean(simg), np.sum(simg))






