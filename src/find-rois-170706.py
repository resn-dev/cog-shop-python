import numpy as np
import cv2
from matplotlib import pyplot as plt
import pytesseract
from PIL import Image
import re

#~ in ipython
#~ cv2.startWindowThread()

#~ ##################################################################### analysis, processing
def find_chars(some):
    

def find_texts(thresh):
    #~ kernel
    #~ kernel = cv2.getStructuringElement(cv2.MORPH_CROSS, (3,3))
    kernel = cv2.getStructuringElement(cv2.MORPH_CROSS, (4,3))
    #~ dilate
    #~ dilated = cv2.dilate(thresh, kernel, iterations=13)
    dilated = cv2.dilate(thresh, kernel, iterations=6)
    # get contours
    contours, hierarchy = cv2.findContours(dilated, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_NONE)

    outl = list()
    # for each contour found, draw a rectangle around it on original image
    for i, contour in enumerate(contours):
        # get rectangle bounding contour
        [x,y,w,h] = cv2.boundingRect(contour)

        #~ if 150<=w<=220 and 20<=h<=80:
        if 150<=w<=220 and 20<=h<=140:
            outl.append(contour)
            #~ print(x,y,w,h)
    #~ 
    return outl

def find_contours(closed):
    #~ contours,hierarchy = cv2.findContours(thresh, 1, 2)

    #~ cnt = contours[0]
    #~ M = cv2.moments(cnt)
    #~ print M
    
    (cnts, _) = cv2.findContours(closed.copy(), cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
    total = 0
    outl = list()
    # loop over the contours
    for i,c in enumerate(cnts):
        # approximate the contour
        peri = cv2.arcLength(c, True)
        approx = cv2.approxPolyDP(c, 0.02 * peri, True)
        # if the approximated contour has four points/vertices, then its a rectangle, and
        # test the length of the contour, threshold when more than half the product box is visible
        if len(approx) == 4 and peri >= 800:
            x1,x2 = [b for a in approx[1:3] for b in a]
            #~ filter out left menu objects
            if x1[0] > 450:
                #~ print(x2[0]-x1[0])
                #~ cv2.drawContours(image, [approx], -1, (0, 255, 0), 4)
                #~ mask = np.zeros_like(closed)
                #~ cv2.drawContours(mask, [approx], -1, 255, -1)
                #~ out = np.zeros_like(closed)
                #~ out[mask == 255] = closed[mask == 255]
                
                #~ x,y,w,h = cv2.boundingRect(approx)
                #~ out = closed[y:y+h, x:x+w]
                #~ s = 'tmp/' + "%05d" % i + '.png' 
                #~ cv2.imwrite(s, out)
                
                #~ save good boxes
                outl.append(approx)
                total += 1
    #~ print(total)

    #~ return closed
    return outl

def find_edges(gray):
    edged = cv2.Canny(gray, 10, 250)
    #~ kernel = cv2.getStructuringElement(cv2.MORPH_RECT, (7, 7))
    kernel = cv2.getStructuringElement(cv2.MORPH_RECT, (5, 5))
    closed = cv2.morphologyEx(edged, cv2.MORPH_CLOSE, kernel)
    #~ 
    #~ return edged
    return closed

#~ ##################################################################### visualizations
def draw_boxes(image, boxes, texts):
    for b in boxes:
        #~ outside box
        cv2.drawContours(image, [b], -1, (0, 255, 0), 4)
        bx,by,bw,bh = cv2.boundingRect(b)
        #~ inside boxes
        for t in texts:
            tx,ty,tw,th = cv2.boundingRect(t)
            
            if bx<=tx<=(bx+bw) and by<=ty<=(by+bh):
                #~ text in box
                cv2.circle(image, (tx,ty), 10, (0,0,0), 4)
                
                #~ top box, image
                if (ty-220)>62:#190
                    #~ image points
                    ipt1 = (bx,by)
                    ipt2 = (bx+bw,by)
                    ipt3 = (bx+bw,by+220)
                    ipt4 = (bx,by+220)
                    #~ draw box
                    cv2.drawContours(image, [np.array([ipt1,ipt2,ipt3,ipt4])], -1, (255, 0, 0), 2)
                
                #~ bottom box, text
                #~ if ((ty+(bh-ty))<1005 and (ty-by)>150) or ((ty+(bh-ty))<1005 and 62<=ty<=190):
                if (ty+(bh-ty))<1005 and ((ty-by)>150 or 62<=ty<=190) and ((by+bh)-ty)>100:
                    #~ text points
                    tpt1 = (bx,ty)
                    tpt2 = (bx+bw,ty)
                    tpt3 = (bx+bw,by+bh)
                    tpt4 = (bx,by+bh)
                    #~ draw box
                    cv2.drawContours(image, [np.array([tpt1,tpt2,tpt3,tpt4])], -1, (0, 0, 255), 2)
    #~ 
    return image

def draw_lines(image):
    #~ top
    cv2.line(image, (100,62), (1500,62), (255,255,255), 2)
    for i in [471, 710, 951, 1191, 1431]:
        cv2.line(image, (i,60), (i,64), (0,0,0), 10)
    #~ bottom
    cv2.line(image, (100,1005), (1500,1005), (255,255,255), 2)
    for i in [471, 710, 951, 1191, 1431]:
        cv2.line(image, (i,1000), (i,1010), (0,0,0), 10)
    #~ 
    return image

#~ #####################################################################
def analyze_video():
    cap = cv2.VideoCapture('vid/31-e2ec8c29-bgscrrec.mkv')

    while(cap.isOpened()):
        ret, frame = cap.read()
        image = frame.copy()

        image = analyze_frame(image)
        #~ gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
        #~ image = gray

        cv2.imshow('frame', image)
        if cv2.waitKey(100) & 0xFF == ord('q'):
            s = 'tmp/' + "%05d" % np.random.randint(1000) + '.png' 
            cv2.imwrite(s, frame)
            break

    cap.release()
    cv2.destroyAllWindows()

def analyze_frame(frame=None, testing=False):
    if frame is None:
        testing = True
        #~ frame = cv2.imread('img/00187.png')
        #~ frame = cv2.imread('tmp/00839.png')
        #~ frame = cv2.imread('tmp/00376.png')
        frame = cv2.imread('tmp/00765.png')
    some = draw_lines(frame)
    gray = cv2.cvtColor(some, cv2.COLOR_BGR2GRAY)
    gray = cv2.GaussianBlur(gray, (3, 3), 0)
    #~ frame = gray

    #~ image = find_objects(frame)
    #~ image = cv2.threshold(frame, 245, 255, cv2.THRESH_BINARY)[1]
    #~ ret,thresh = cv2.threshold(img,127,255,0)
    #~ contours,hierarchy = cv2.findContours(thresh, 1, 2)
    #~ ret, image = cv2.threshold(gray,0,255,cv2.THRESH_BINARY_INV+cv2.THRESH_OTSU)
    #~ ret, image = cv2.threshold(frame, 245, 255, cv2.THRESH_BINARY)
    #~ image = image_segmentation(image)
    #~ ret, image = cv2.threshold(frame, 245, 255, cv2.THRESH_BINARY)
    #~ find_contours(image)
    
    ret, image = cv2.threshold(gray, 245, 255, cv2.THRESH_BINARY)
    image = find_edges(image)
    boxes = find_contours(image)
    
    gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
    _,thresh = cv2.threshold(gray, 150,255, cv2.THRESH_BINARY_INV)
    texts = find_texts(thresh)
    chars = find_chars(texts)
    
    image = draw_boxes(frame, boxes, texts)
    #~ image = gray

    if testing:
        #~ cv2.imwrite('tmp/test.png', image)
        cv2.imshow('frame', image)
        cv2.waitKey(0)
        #~ plt.imshow(image),plt.show()
    else:
        #~ 
        return image

def analyze_test():
    frame = cv2.imread('img/00839.png')
    some = draw_lines(frame)
    gray = cv2.cvtColor(some, cv2.COLOR_BGR2GRAY)
    cv2.imshow('frame', gray)
    cv2.waitKey(0)

#~ #####################################################################
def main():
    pass

if __name__ == '__main__':
    #~ 
    analyze_frame()
    #~ analyze_video()















