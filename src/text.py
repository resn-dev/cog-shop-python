import cv2

image = cv2.imread("img/Screenshot_2017-02-01_14-45-47.png")
# grayscale
gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
# threshold
_,thresh = cv2.threshold(gray, 150,255, cv2.THRESH_BINARY_INV)

kernel = cv2.getStructuringElement(cv2.MORPH_CROSS, (3,3))
# dilate
dilated = cv2.dilate(thresh, kernel, iterations=13)
contours, hierarchy = cv2.findContours(dilated, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_NONE) # get contours

# for each contour found, draw a rectangle around it on original image
for contour in contours:
    # get rectangle bounding contour
    [x,y,w,h] = cv2.boundingRect(contour)

    # discard areas that are too large
    if h>300 and w>300:
        continue

    # discard areas that are too small
    if h<60 or w<60:
        continue

    # draw rectangle around contour on original image
    cv2.rectangle(image, (x,y), (x+w,y+h), (255,0,255), 2)

    #you can crop image and send to OCR  , false detected will return no text :)
    #~ cropped = img[y :y +  h , x : x + w]
    #~ s = file_name + '/crop_' + str(index) + '.jpg' 
    #~ cv2.imwrite(s , cropped)
    #~ index = index + 1

# write original image with added contours to disk 
#~ cv2.imwrite("contoured.jpg", image) 
cv2.imshow('captcha_result' , image)
cv2.waitKey()

if False:
    import cv2
    import numpy as np
    def captch_ex(file_name ):
        img  = cv2.imread(file_name)

        img_final = cv2.imread(file_name)
        img2gray = cv2.cvtColor(img,cv2.COLOR_BGR2GRAY)
        ret, mask = cv2.threshold(img2gray, 180, 255, cv2.THRESH_BINARY)
        image_final = cv2.bitwise_and(img2gray , img2gray , mask =  mask)
        ret, new_img = cv2.threshold(image_final, 180 , 255, cv2.THRESH_BINARY)  # for black text , cv.THRESH_BINARY_INV
        '''
                line  8 to 12  : Remove noisy portion 
        '''
        # to manipulate the orientation of kernel , large x means horizonatally dilating  more, large y means vertically dilating more 
        kernel = cv2.getStructuringElement(cv2.MORPH_CROSS, (3, 3))
        kernel = cv2.getStructuringElement(cv2.MORPH_ELLIPSE, (3, 3))
        #~ morphologyEx(small, grad, cv2.MORPH_GRADIENT, kernel)
        # dilate , more the iteration more the dilation
        dilated = cv2.dilate(new_img,kernel,iterations=90)

        contours, hierarchy = cv2.findContours(dilated,cv2.RETR_EXTERNAL,cv2.CHAIN_APPROX_NONE)
        index = 0 
        for contour in contours:
            # get rectangle bounding contour
            [x,y,w,h] = cv2.boundingRect(contour)

            #Don't plot small false positives that aren't text
            #~ if w<50 and h<50:
                #~ continue

            # draw rectangle around contour on original image
            cv2.rectangle(img,(x,y),(x+w,y+h),(255,0,255),2)

            '''
            #you can crop image and send to OCR  , false detected will return no text :)
            cropped = img_final[y :y +  h , x : x + w]

            s = file_name + '/crop_' + str(index) + '.jpg' 
            cv2.imwrite(s , cropped)
            index = index + 1

            '''
        # write original image with added contours to disk  
        cv2.imshow('captcha_result' , img)
        #~ cv2.imshow('captcha_result' , new_img)
        cv2.waitKey()


    file_name ='img/Screenshot_2017-02-01_14-45-47.png'
    captch_ex(file_name)
