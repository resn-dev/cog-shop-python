import numpy as np
import cv2

# Create a black image
#~ img = np.zeros((512,512,3), np.uint8)
img = np.ones((512,512,3), np.uint8)
# Draw a diagonal blue line with thickness of 5 px
img = cv2.line(img,(0,0),(511,511),(255,0,0),5)

img = cv2.rectangle(img,(384,0),(510,128),(0,255,0),3)
img = cv2.circle(img,(447,63), 63, (0,0,255), -1)

height,width = [32,32]
blank_image = np.zeros((height,width,3), np.uint8)
#~ blank_image[:,0:10] = (255,0,0)
#~ blank_image[:,0:10] = (0,255,0)
#~ blank_image[:,0:10] = (0,0,255)
blank_image[:,:,:] = (255,255,255)
#~ blank_image[:,:,:] = (0,255,0)
#~ blank_image[:,:,:] = (0,0,255)

blank_image = cv2.rectangle(blank_image, (4,4), (27,27), (0,0,255), -1)
blank_image = cv2.line(blank_image, (4,4), (27,27), (255,255,255), 4)
blank_image = cv2.line(blank_image, (4,27), (27,4), (255,255,255), 4)

cv2.imwrite('tmp/gaze-point.png', blank_image)


