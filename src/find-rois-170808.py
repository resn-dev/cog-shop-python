import numpy as np
import cv2
#~ import math
from matplotlib import pyplot as plt
import pytesseract
from PIL import Image
#~ import re
import csv
#~ from collections import Counter

#~ in ipython
#~ cv2.startWindowThread()

#~ ##################################################################### analysis, processing
def find_chars(image, texts):
    outl = list()
    for i,t in enumerate(texts):
        # get rectangle bounding contour
        [x,y,w,h] = cv2.boundingRect(t)

        # crop image 
        cropped = image[y:y+h, x:x+w]
        #~ s = 'tmp/chars-' + "%05d" % i + '.png' 
        #~ cv2.imwrite(s, cropped)

        #~ if False:
        #~ ocr
        #~ text = pytesseract.image_to_string(Image.open(s), lang='eng')#lang='swe')
        chars = pytesseract.image_to_string(Image.fromarray(cropped), lang='eng')#lang='swe')
        #~ print(i, char)
        outl.append(chars)
    #~ 
    return outl

def find_texts(thresh):
    #~ kernel
    #~ kernel = cv2.getStructuringElement(cv2.MORPH_CROSS, (3,3))
    kernel = cv2.getStructuringElement(cv2.MORPH_CROSS, (4,3))
    #~ dilate
    #~ dilated = cv2.dilate(thresh, kernel, iterations=13)
    dilated = cv2.dilate(thresh, kernel, iterations=6)
    # get contours
    contours, hierarchy = cv2.findContours(dilated, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_NONE)

    outl = list()
    # for each contour found, draw a rectangle around it on original image
    for i, contour in enumerate(contours):
        # get rectangle bounding contour
        [x,y,w,h] = cv2.boundingRect(contour)

        #~ if 150<=w<=220 and 20<=h<=80:
        if 150<=w<=220 and 20<=h<=140:
            outl.append(contour)
            #~ print(x,y,w,h)
    #~ 
    return outl

def find_contours(closed):
    #~ contours,hierarchy = cv2.findContours(thresh, 1, 2)

    #~ cnt = contours[0]
    #~ M = cv2.moments(cnt)
    #~ print M
    
    (cnts, _) = cv2.findContours(closed.copy(), cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
    total = 0
    outl = list()
    # loop over the contours
    for i,c in enumerate(cnts):
        # approximate the contour
        peri = cv2.arcLength(c, True)
        approx = cv2.approxPolyDP(c, 0.02 * peri, True)
        # if the approximated contour has four points/vertices, then its a rectangle, and
        # test the length of the contour, threshold when more than half the product box is visible
        if len(approx) == 4 and peri >= 800:
            x1,x2 = [b for a in approx[1:3] for b in a]
            #~ filter out left menu objects
            if x1[0] > 450:
                #~ print(x2[0]-x1[0])
                #~ cv2.drawContours(image, [approx], -1, (0, 255, 0), 4)
                #~ mask = np.zeros_like(closed)
                #~ cv2.drawContours(mask, [approx], -1, 255, -1)
                #~ out = np.zeros_like(closed)
                #~ out[mask == 255] = closed[mask == 255]
                
                #~ x,y,w,h = cv2.boundingRect(approx)
                #~ out = closed[y:y+h, x:x+w]
                #~ s = 'tmp/' + "%05d" % i + '.png' 
                #~ cv2.imwrite(s, out)
                
                #~ save good boxes
                outl.append(approx)
                total += 1
    #~ print(total)

    #~ return closed
    return outl

def find_edges(gray):
    edged = cv2.Canny(gray, 10, 250)
    #~ kernel = cv2.getStructuringElement(cv2.MORPH_RECT, (7, 7))
    kernel = cv2.getStructuringElement(cv2.MORPH_RECT, (5, 5))
    closed = cv2.morphologyEx(edged, cv2.MORPH_CLOSE, kernel)
    #~ 
    #~ return edged
    return closed

#~ ##################################################################### visualizations
def draw_texts(fnum, image, texts):
    #~ get y values of text objects
    outl = list()
    for i,t in enumerate(texts):
        tx,ty,tw,th = cv2.boundingRect(t)
        #~ round y value to nearest 10
        outl.append(int(round(ty, -1)))
    #~ analyze y values
    v,i,c = np.unique(outl, return_counts=True, return_index=True)
    #~ get most common y values
    cval = [v[j] for j,k in enumerate(c) if k>2]
    #~ get all indices of common values
    il = [i for i,y in enumerate(outl) if y in cval]
    #~ get text object by indices
    if len(il)==0:
        il = [False]
    texts = np.array(texts)[np.array(il)]
    
    for i,t in enumerate(texts):
        tx,ty,tw,th = cv2.boundingRect(t)
        
        #~ if ((ty+(bh-ty))<1005 and (ty-by)>150) or ((ty+(bh-ty))<1005 and 62<=ty<=190):
        #~ if (ty+(bh-ty))<1005 and ((ty-by)>150 or 62<=ty<=190) and ((by+bh)-ty)>100:
        if tx>450 and 62<=ty<=1005:
            #~ text points
            tpt1 = (tx,ty)
            tpt2 = (tx+tw,ty)
            tpt3 = (tx+tw,ty+th)
            tpt4 = (tx,ty+th)
            #~ draw box
            cv2.drawContours(image, [np.array([tpt1,tpt2,tpt3,tpt4])], -1, (0,255,0), 2)
    #~ 
    return image

def draw_objects(fnum, image, boxes, texts, chars):
    #~ output list
    bdata = list()
    for i,b in enumerate(boxes):
        #~ outside box
        cv2.drawContours(image, [b], -1, (0,255,0), 4)
        bx,by,bw,bh = cv2.boundingRect(b)
        #~ inside boxes
        for j,t in enumerate(texts):
            tx,ty,tw,th = cv2.boundingRect(t)
            
            if bx<=tx<=(bx+bw) and by<=ty<=(by+bh):
                #~ text in box
                cv2.circle(image, (tx,ty), 10, (0,0,0), 4)
                clen = 20 if len(chars[j])>=20 else len(chars[j])
                boxid = chars[j][:clen]
                cv2.putText(image, boxid, (tx+10,ty-10), cv2.FONT_HERSHEY_SIMPLEX, 0.6, (150,150,150), 2)
                
                #~ top box, image
                if (ty-220)>62:#190
                    #~ image points
                    ipt1 = (bx,by)
                    ipt2 = (bx+bw,by)
                    ipt3 = (bx+bw,by+220)
                    ipt4 = (bx,by+220)
                    #~ draw box
                    cv2.drawContours(image, [np.array([ipt1,ipt2,ipt3,ipt4])], -1, (255,0,0), 2)
                
                #~ bottom box, text
                #~ if ((ty+(bh-ty))<1005 and (ty-by)>150) or ((ty+(bh-ty))<1005 and 62<=ty<=190):
                if (ty+(bh-ty))<1005 and ((ty-by)>150 or 62<=ty<=190) and ((by+bh)-ty)>100:
                    #~ text points
                    tpt1 = (bx,ty)
                    tpt2 = (bx+bw,ty)
                    tpt3 = (bx+bw,by+bh)
                    tpt4 = (bx,by+bh)
                    #~ draw box
                    cv2.drawContours(image, [np.array([tpt1,tpt2,tpt3,tpt4])], -1, (0,0,255), 2)

                #~ box data
                bdata.append([fnum, boxid, bx,by,bw,bh])            
    #~ 
    return [image, bdata]

def draw_lines(image):
    #~ top
    cv2.line(image, (100,62), (1500,62), (255,255,255), 2)
    for i in [471, 710, 951, 1191, 1431]:
        cv2.line(image, (i,60), (i,64), (0,0,0), 10)
    #~ bottom
    cv2.line(image, (100,1005), (1500,1005), (255,255,255), 2)
    for i in [471, 710, 951, 1191, 1431]:
        cv2.line(image, (i,1000), (i,1010), (0,0,0), 10)
    #~ 
    return image

#~ ##################################################################### template matching
def get_texts(fnum, image, texts):
    pass
    

#~ #####################################################################
def analyze_video():
    cap = cv2.VideoCapture('vid/31-e2ec8c29-bgscrrec.mkv')

    fnum = 0
    while(cap.isOpened()):
        #~ current_frame_flag = cv2.cv.CV_CAP_PROP_POS_FRAMES
        #~ total_frames_flag = cv2.cv.CV_CAP_PROP_FRAME_COUNT
        fnum += 1
        ret, frame = cap.read()
        image = frame.copy()

        image = analyze_frame(fnum, image)
        #~ gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
        #~ image = gray

        #~ new ###################
        key = cv2.waitKey(10) & 0xff
        if not ret:
            break
        if key == ord('p'):
            while True:
                key2 = cv2.waitKey(1) or 0xff
                cv2.imshow('frame', image)
                if key2 == ord('p'):
                    break
        cv2.imshow('frame', image)
        if key == 27:
            #~ print(fnum)
            break

        #~ old ###################
        #~ cv2.imshow('frame', image)
        #~ if cv2.waitKey(100) & 0xFF == ord('q'):
            #~ s = 'tmp/' + "%05d" % np.random.randint(1000) + '.png' 
            #~ cv2.imwrite(s, frame)
            #~ break

    cap.release()
    cv2.destroyAllWindows()

def analyze_frame(fnum=0, frame=None, testing=False):
    if frame is None:
        testing = True
        #~ frame = cv2.imread('img/00187.png')
        #~ frame = cv2.imread('tmp/00839.png')
        #~ frame = cv2.imread('tmp/00376.png')
        #~ frame = cv2.imread('tmp/00765.png')
        #~ frame = cv2.imread('tmp/00914.png')
        frame = cv2.imread('tmp/00376.png')
    
    #~ prepare image
    #~ some = draw_lines(frame)
    #~ gray = cv2.cvtColor(some, cv2.COLOR_BGR2GRAY)
    #~ gray = cv2.GaussianBlur(gray, (3,3), 0)
    
    #~ find product aois
    #~ ret, image = cv2.threshold(gray, 245, 255, cv2.THRESH_BINARY)
    #~ image = find_edges(image)
    #~ boxes = find_contours(image)
    
    #~ find texts and chars
    gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
    _,thresh = cv2.threshold(gray, 150,255, cv2.THRESH_BINARY_INV)
    texts = find_texts(thresh)
    #~ chars = find_chars(gray, texts)
    
    #~ draw results
    #~ image,bdata = draw_objects(fnum, frame, boxes, texts, chars)
    #~ image = draw_texts(fnum, frame, texts)
    
    #~ template matching
    get_texts(fnum, frame, texts)
    image = match_texts(fnum, frame, texts)

    #~ handle output
    #~ with open("output.csv", "wb") as f:
    #~ with open('tmp/output.csv', 'a') as f:
        #~ writer = csv.writer(f)
        #~ writer.writerows(bdata)
    #~ print(bdata)

    if testing:
        #~ cv2.imwrite('img/test.png', image)
        cv2.imshow('frame', image)
        cv2.waitKey(0)
        #~ plt.imshow(image),plt.show()
    else:
        #~ 
        return image

def analyze_output():
    frame = cv2.imread('img/00839.png')
    some = draw_lines(frame)
    gray = cv2.cvtColor(some, cv2.COLOR_BGR2GRAY)
    cv2.imshow('frame', gray)
    cv2.waitKey(0)

#~ #####################################################################
def main():
    pass

if __name__ == '__main__':
    #~ 
    analyze_frame()
    #~ analyze_video()

    #~ analyze_output()













