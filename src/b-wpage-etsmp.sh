#data dir
dd="/home/sol-nhl/get/cog-shop/170502-txt/"
#trial, product category
for t in {c,y,p}; do
#trial start time
egrep -v '^p|\s0\s0' csv/video/study-b-trial-"$t".tsv | while IFS=$'\t' read p s c e d; do 
#define data file
f="$dd$p.txt"; echo $f;
# get instruction for task
a=$(egrep -n x1050 "$f" | tail -n +4 | head -n 1 | cut -f1 -d':')
# get starting point of surf / screen recording
b=$(tail -n +$a "$f" | egrep -n jpg | head -n2 | tail -n1 | cut -f1 -d':'); c=$(( $a + $b - 1 ))
#find trial start relative line number, lower bound
tsl=$(tail -n +$c "$f" | egrep -i '(smp|jpg)' | sed "$s"'i trial' | egrep -n 'jpg|trial' | egrep -C1 'trial' | head -n 1 | tr -d '[:space:]' | cut -f1 -d ':')
#find trial start relative line number, upper bound
tsu=$(tail -n +$c "$f" | egrep -i '(smp|jpg)' | sed "$s"'i trial' | egrep -n 'jpg|trial' | egrep -C1 'trial' | tail -n 1 | tr -d '[:space:]' | cut -f1 -d ':')
#compare trial start, lower bound is generally closer
echo "video coding: $s" "| lower bound: $tsl" $(( $s - $tsl )) "| upper bound: $tsu" $(( $tsu - $s ))
#find trial start absolute line number, lower bound
tsa=$(tail -n +$c "$f" | egrep -i '(smp|jpg)' | sed "$s"'i trial' | egrep -n 'jpg|trial' | egrep -C1 'trial' | head -n 1 | tr -d '[:space:]' | cut -f1,3 -d ':' | while IFS=':' read tsn tsi; do egrep -n "$tsi" "$f"; done)
#get trial start vars
tsn=$(echo "$tsa" | tr -d '[:space:]' | cut -f1 -d':')
tsi=$(echo "$tsa" | tr -d '[:space:]' | cut -f3 -d':')
#get trial end, start + duration
#ten=$(( $tsn + $d ))
ten=$(echo "$tsn + $d" | tr -d '\r' | bc)
#trial start image, manual inspection
#find /run/user/1000/gvfs/smb-share\:server\=htahml-srv2.uw.lu.se\,share\=1702-02\$/KNA180214/ -iname "*$tsi*" -exec cp {} /tmp/cog/trial-images/"$t"-"$p"-"$tsi" \;
#~ omit while testing
if false; then
#exclude set, participants
if [[ "$t-$p" =~ ^(c-252|p-252|y-252)$ ]]; then
echo "excluding participant $t-$p"
else
# extract et samples
sed -n "$tsn","$ten"p "$f" | tr -d '\r' > ~/get/cog-shop/csv/study-b/"$p"-"$t".txt
# reduce, split data
cut -f1,4,22,23 ~/get/cog-shop/csv/study-b/"$p"-"$t".txt | egrep -v '^$' | csplit - /scroll/ {*} && rm tmp/scroll.csv
# add column for scroll value
for i in x*; do a=$(head -n1 $i | sed 's/^/@/g'); sed -e "s/$/\t$a/g" "$i" >> tmp/scroll.csv; done && rm xx*
# clean up dataset
cat <(echo -e "time\tlraw\tlporx\tlpory\tscroll\tmsg") tmp/scroll.csv | \
# convert to lower case
tr '[:upper:]' '[:lower:]' | \
# push messages to final column
sed 's/#/\t\t\t\t#/g' | \
# replace nonsense scroll values in first split segment
sed 's/@.*scroll/@scroll/g' | \
sed 's/@[0-9].*$/@scroll 0 0 0/g' | \
# get relevant fields
cut -f1,3-6 | \
# extract scroll values, delete context (x scroll can be other than 0)
sed 's/@scroll -\?[0-9]\{1,6\} //g' | \
# extract scroll values, delete context (possibly z values (page zoom?) other than 0 also?)
sed 's/ 0$/\t0/g' | \
# remove extra message field
sed 's/l \(-\?[0-9]\{1,6\}\) \(-\?[0-9]\{1,6\}\)\t0/l \1 \2 0/g' > /home/niels/rnd/d/python/opencv/cog-shop/csv/study-b/etsmp/"$p"-"$t"r.txt
#~ omit while testing
fi
fi
#
done
done
