import numpy as np
import cv2

#~ in ipython
#~ cv2.startWindowThread()

def find_objects(image):
    # grayscale
    gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
    # threshold
    _,thresh = cv2.threshold(gray, 150,255, cv2.THRESH_BINARY_INV)

    kernel = cv2.getStructuringElement(cv2.MORPH_CROSS, (3,3))
    # dilate
    dilated = cv2.dilate(thresh, kernel, iterations=13)
    contours, hierarchy = cv2.findContours(dilated, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_NONE) # get contours

    # for each contour found, draw a rectangle around it on original image
    for i, contour in enumerate(contours):
        # get rectangle bounding contour
        [x,y,w,h] = cv2.boundingRect(contour)

        # discard areas that are too large
        #~ if h>500 and w>300:
            #~ continue

        # discard areas that are too small
        #~ if h<600 or w<400:
            #~ continue

        if (100 < w < 300) and (300 < h < 600):
            #~ continue

            # draw rectangle around contour on original image
            cv2.rectangle(image, (x, y), (x+w, y+h), (0,255,0), 2)

    return image

#~ #####################################################################
cap = cv2.VideoCapture('vid/31-e2ec8c29-bgscrrec.mkv')

while(cap.isOpened()):
    ret, frame = cap.read()

    #~ image = find_objects(frame)
    gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
    image = gray

    cv2.imshow('frame', image)
    if cv2.waitKey(100) & 0xFF == ord('q'):
        break

    s = 'tmp/' + "%05d" % np.random.randint(1000) + '.png' 
    #~ cv2.imwrite(s, image)

cap.release()
cv2.destroyAllWindows()
