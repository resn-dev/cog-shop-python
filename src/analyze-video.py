import numpy as np
import cv2
import pytesseract
from PIL import Image
import re

#~ #####################################################################
#~ analyze video frames
#~ detect product text areas
#~ crop product texts
#~ save product texts
#~ ocr product text to get product id
#~ save area position on each frame to file
#~ time, frame id, product id, position

#~ analyze gaze samples
#~ time, position
#~ determine if gaze position overlaps product position at time t

#~ image = cv2.imread("img/Screenshot_2017-02-01_14-45-47.png")
image = cv2.imread('img/00187.png')
# grayscale
gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
# threshold
#~ _,thresh = cv2.threshold(gray, 150,255, cv2.THRESH_BINARY_INV)
_,thresh = cv2.threshold(gray, 150,255, cv2.THRESH_BINARY_INV)
#~ kernel
#~ kernel = cv2.getStructuringElement(cv2.MORPH_CROSS, (3,3))
kernel = cv2.getStructuringElement(cv2.MORPH_CROSS, (4,3))
# dilate
#~ dilated = cv2.dilate(thresh, kernel, iterations=13)
dilated = cv2.dilate(thresh, kernel, iterations=6)
# get contours
contours, hierarchy = cv2.findContours(dilated, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_NONE)

# for each contour found, draw a rectangle around it on original image
for i, contour in enumerate(contours):
    # get rectangle bounding contour
    [x,y,w,h] = cv2.boundingRect(contour)

    # discard areas that are too large
    #~ if h>500 and w>500:
        #~ continue
    # discard areas that are too small
    #~ if h<60 or w<60:
        #~ continue
    #~ if w<150 or h>80:
        #~ continue

    if 150<=w<=220 and 20<=h<=80:
        # draw rectangle around contour on original image
        cv2.rectangle(image, (x, y), (x+w, y+h), (0,255,0), 2)
        print(y)

    if False:
        # crop image 
        cropped = image[y:y+h, x:x+w]
        s = 'img/' + "%05d" % i + '.png' 
        cv2.imwrite(s, cropped)
        print(s)

        #~ ocr
        text = pytesseract.image_to_string(Image.open(s), lang='eng')#lang='swe')
        #~ text = re.sub('(Banana)', r'\1Toothpaste', oldstring)
        p = r"jfr"
        p = re.compile('(\s)kr')
        m = p.search(text)
        if m is None: n = -1
        else: n = m.end()
        text = text[:n]
        font = cv2.FONT_HERSHEY_SIMPLEX
        cv2.putText(image, text, (x,y), font, 0.4, (0,0,0), 2)
        #~ print(text)

# write original image with added contours to disk 
cv2.imwrite('img/auto-detect-products.png', image) 
cv2.imshow('auto-detect-product', image)
#~ cv2.imshow('auto-detect-product', thresh)
cv2.waitKey()


