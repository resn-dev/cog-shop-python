import numpy as np
import cv2
import argparse

#~ methods = [
#~ 'cv2.TM_CCOEFF', 
#~ 'cv2.TM_CCOEFF_NORMED', 
#~ 'cv2.TM_CCORR',
#~ 'cv2.TM_CCORR_NORMED', 
#~ 'cv2.TM_SQDIFF', 
#~ 'cv2.TM_SQDIFF_NORMED']

def some_func():
    (h,w) = t.shape[:2]
    temp = cv2.cvtColor(t, cv2.COLOR_BGR2GRAY)
    result = cv2.matchTemplate(gray, temp, cv2.TM_CCOEFF)
    min_val,max_val,min_loc,max_loc = cv2.minMaxLoc(result)
    xy = max_loc
    wh = (xy[0]+w, xy[1]+h)
    cv2.rectangle(image, xy, wh, (0,0,255), 2)
    #~ draw aoi
    y1 = 62 if xy[1]-200<=0 else xy[1]-200
    y2 = 1005 if xy[1]+200>=1005 else xy[1]+200
    cv2.rectangle(image, (xy[0],y1), (xy[0]+w,y2), (0,255,0), 2)

#~ ##################################################################### analyses
def analyze_frame(fnum=0, frame=None, testing=False):
    if frame is None:
        testing = True

    #~ background web page image
    bimg = cv2.imread('img/allsylt2.png')
    bgim = cv2.cvtColor(bimg, cv2.COLOR_BGR2GRAY)
    #~ frame image
    #~ fimg = cv2.imread('tmp/00914.png', 0)
    #~ fimg = cv2.imread('tmp/00765.png')
    fgim = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
    #~ crop: 62, 450
    cgim = fgim[62:1000, 460:1450]

    #~ find frame location on background
    (h,w) = cgim.shape[:2]
    #~ result = cv2.matchTemplate(bgim, cgim, cv2.TM_CCOEFF)
    #~ min_val,max_val,min_loc,max_loc = cv2.minMaxLoc(result)
    #~ xy = max_loc
    #~ wh = (xy[0]+w, xy[1]+h)
    #~ with threshold
    res = cv2.matchTemplate(bgim, cgim, cv2.TM_CCOEFF_NORMED)
    threshold = 0.8
    loc = np.where(res >= threshold)
    if loc[0].any():
        #~ xy = max_loc
        #~ wh = (xy[0]+w, xy[1]+h)
        print(loc, fnum%10)

        #~ draw frame broder on bg image
        #~ cv2.rectangle(bimg, (xy), (wh), (0,255,0), 5)

    #~ resize about factor 0.25
    r = 1000.0 / bimg.shape[0]
    dim = (int(bimg.shape[1] * r), 1000)
    rimg = cv2.resize(bimg, dim, interpolation=cv2.INTER_AREA)

    return rimg

def analyze_video(posl=[216,20000]):
    fs,fe = posl

    cap = cv2.VideoCapture('vid/31-e2ec8c29-bgscrrec.mkv')
    #~ cap = cv2.VideoCapture('/home/sol-nhl/get/cog-shop/13-e2ec8c29-bgscrrec1.mkv')
    #~ cap.set(cv2.CAP_PROP_POS_MSEC, 25000)
    #~ cap.set(cv2.cv.CV_CAP_PROP_POS_MSEC, ts)
    cap.set(cv2.cv.CV_CAP_PROP_POS_FRAMES, fs)
    
    #~ fnum = 0
    fcnt = cap.get(cv2.cv.CV_CAP_PROP_FRAME_COUNT)
    fnum = cap.get(cv2.cv.CV_CAP_PROP_POS_FRAMES)
    while(cap.isOpened() and fnum<fcnt):
        
        #~ fnum += 1
        fnum = cap.get(cv2.cv.CV_CAP_PROP_POS_FRAMES)
        #~ print(fnum)
        ret, frame = cap.read()
        image = frame.copy()
        
        #~ perform image analysis
        image = analyze_frame(fnum, image)
        
        #~ new ###################
        key = cv2.waitKey(100) & 0xff
        if not ret:
            break
        if key == ord('p'):
            while True:
                key2 = cv2.waitKey(1) or 0xff
                cv2.imshow('frame', image)
                if key2 == ord('p'):
                    break
        cv2.imshow('frame', image)
        if key == 27:
            #~ print(fnum)
            break

    cap.release()
    cv2.destroyAllWindows()

#~ ##################################################################### main
def main():
    pass

if __name__ == '__main__':
    ap = argparse.ArgumentParser()
    ap.add_argument('-a', '--action', help='select action')
    args = vars(ap.parse_args())

    #~ analyze_frame()
    if args['action'] == 'video':
        analyze_video()
    elif args['action'] == 'match':
        analyze_temps()
    else:
        exit('please provide a valid argument')



