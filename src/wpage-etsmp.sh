for c in {c,y,p}; do
egrep -v '^t|0\s0\s0|\s\s$' csv/video/coding-images-"$c".csv | while IFS=$'\t' read c s b x e i1 i2; do 
#~ egrep '\s(21|22)\s' csv/video/coding-"$c".csv | egrep -v '0\s0\s0' | while IFS=$'\t' read c s b x e; do 
#~ subject, manual testing
#~ c="p";s="10"
s=$(printf %02d $s)
#~ data file
f="/home/sol-nhl/get/cog-shop/170502-txt/$s.txt"
#~ omit while testing
if false; then
#~ get start of task (instruction stimulus)
n=$(egrep -i -n '1050.jpg' $f | tail -n2 | sed -n '1p' | cut -f1 -d':'); echo $n;
#~ count url events to dynamically set session start
urln=$(tail -n +"$n" $f | tr -d '\r' | egrep -i '1050.jpg|url' | grep -i url | wc | tr -s ' ' | cut -f2 -d' ')
if [ "$urln" == "1" ]; then urln=2; fi
#~ get timestamps for web session interval (url to last instruction)
r=$(tail -n +"$n" $f | tr -d '\r' | egrep -i '1050.jpg|url' | tail -n"$urln" | cut -f1 | tr '\n' '|' | sed 's/|$//g' | sed 's/|.*|/|/g'); echo $r;
#~ get line numbers for web session interval
l=$(egrep -n "$r" $f | cut -f1 -d':' | tr '\n' ',' | sed 's/,$//g'); echo $l;
#~ extract, write session interval
#~ sed -n "$l"p $f | tr -d '\r' > ~/get/cog-shop/csv/"$s"-s.txt
# get trial interval (cereal, yogurt, pasta)
fi
#~ 180202
#~ trial start image trigger
#~ check which image matches trial start best
#~ if [[ ${#i2} -gt 10 ]]; then 
#~ echo "$i2\t$i1"
#~ i1=${i1:3:36}
#~ ts=$(egrep -n "$i1" /home/sol-nhl/get/cog-shop/csv-180202/"$s"-s.txt | cut -f1 -d':'); echo $c $s $b $ts
#~ i2=${i2:3:36}
#~ ts=$(egrep -n "$i2" /home/sol-nhl/get/cog-shop/csv-180202/"$s"-s.txt | cut -f1 -d':'); echo $c $s $b $ts
#~ fi
#~ trial end = trial start ($b) + trial duration samples (26160-22440 = 3720)
i1=${i1:3:36}
ts=$(egrep -n "$i1" "$f" | cut -f1 -d':')
td=$(( $e - $b ))
te=$(( $ts + $td )); echo $ts,$te
#~ sed -n "$b","$e"p ~/get/cog-shop/csv/"$s"-s.txt > ~/get/cog-shop/csv/"$s"-"$c".txt
sed -n "$ts","$te"p "$f" | tr -d '\r' > ~/get/cog-shop/csv/"$s"-"$c".txt
#~ omit while testing
if true; then
# reduce, split data
cut -f1,4,22,23 ~/get/cog-shop/csv/"$s"-"$c".txt | egrep -v '^$' | csplit - /scroll/ {*} && rm tmp/scroll.csv
# add column for scroll value
for i in x*; do a=$(head -n1 $i | sed 's/^/@/g'); sed -e "s/$/\t$a/g" "$i" >> tmp/scroll.csv; done && rm xx*
# clean up dataset
cat <(echo -e "time\tlraw\tlporx\tlpory\tscroll\tmsg") tmp/scroll.csv | \
# convert to lower case
tr '[:upper:]' '[:lower:]' | \
# push messages to final column
sed 's/#/\t\t\t\t#/g' | \
# replace nonsense scroll values in first split segment
sed 's/@.*scroll/@scroll/g' | \
sed 's/@[0-9].*$/@scroll 0 0 0/g' | \
# get relevant fields
cut -f1,3-6 | \
# get scroll values
sed 's/@scroll 0 //g' | \
sed 's/ 0$/\t0/g' | \
# remove extra message fields
sed 's/l 0 \([0-9]\{1,5\}\)\t0/l 0 \1 0/g' > /home/niels/rnd/d/python/opencv/cog-shop/csv/etsmp/"$s"-"$c"r.txt
#~ omit while testing
fi
done
done 
