import numpy as np
import cv2
import pandas as pd
import matplotlib.pyplot as plt
import math
import os.path
import subprocess
import argparse
#~ 
import sys
sys.path.insert(0, './src/pySaliencyMap')
import pySaliencyMap as psal

#~ methods = [
#~ 'cv2.TM_CCOEFF', 
#~ 'cv2.TM_CCOEFF_NORMED', 
#~ 'cv2.TM_CCORR',
#~ 'cv2.TM_CCORR_NORMED', 
#~ 'cv2.TM_SQDIFF', 
#~ 'cv2.TM_SQDIFF_NORMED']

#~ ##################################################################### wpage analyses
def find_contours(closed, image):
    #~ (cnts, _) = cv2.findContours(closed.copy(), cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
    _,cnts,_ = cv2.findContours(closed.copy(), cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
    outl = list()
    # loop over the contours
    for i,c in enumerate(cnts):
        # approximate the contour
        peri = cv2.arcLength(c, True)
        approx = cv2.approxPolyDP(c, 0.02 * peri, True)
        # if approximated contour has four points/vertices, then its a rectangle, and
        # test the length of the contour, threshold when more than half the product box is visible
        if len(approx) == 4 and peri >= 800:
            xy1,xy2 = [b for a in approx[1:3] for b in a]
            #~ filter out objects by position
            if xy1[0] > 450 and xy1[1] > 100:
                #~ print(x1,x2)
                #~ cv2.drawContours(image, [approx], -1, (0, 255, 0), 4)
                #~ outl.append(approx)
                x,y,w,h = cv2.boundingRect(approx)
                #~ cv2.rectangle(image,(x,y),(x+w,y+h),(0,255,0),2)
                outl.append([x,y,w,h])
    #~ return image
    return [outl, image]

def find_edges(gray):
    edged = cv2.Canny(gray, 10, 250)
    #~ kernel = cv2.getStructuringElement(cv2.MORPH_RECT, (7, 7))
    kernel = cv2.getStructuringElement(cv2.MORPH_RECT, (5, 5))
    closed = cv2.morphologyEx(edged, cv2.MORPH_CLOSE, kernel)
    #~ 
    #~ return edged
    return closed
    
def get_context(image, aoi):
    x,y,w,h = aoi
    #~ cv2.circle(image, (x,y), 10, (0,0,0), 4)
    #~ draw aoi, ternary logic
    #~ y1 = y if y-300<=200 else y-300
    #~ y2 = image.shape[0] if y+600>=image.shape[0] else y+600
    if 100<=y<=300:
        #~ top row
        y1,y2 = y,y+900
    elif y+600>image.shape[0]:
        #~ bottom row
        y1,y2 = y-600,image.shape[0]
    else:
        #~ middle rows
        y1,y2 = y-300,y+600
    #~ x1,x2 = 460-300,1450-300+j*10
    #~ cv2.rectangle(image, (x1,y1), (x2,y2), (0,0,255), 2)
    #~ saliency
    x1,x2 = 460,1450
    aimg = image[y:y+h, x:x+w]
    fimg = image[y1:y2, x1:x2]
    #~ add border ###################
    g = aimg
    row,col= g.shape[:2]
    bottom = g[row-2:row, 0:col]
    mean = cv2.mean(bottom)[0]
    bordersize = 100
    #~ imgb
    b = cv2.copyMakeBorder(g, 
    top=bordersize, 
    bottom=bordersize, 
    left=bordersize, 
    right=bordersize, 
    borderType=cv2.BORDER_CONSTANT, 
    #~ value=[mean,mean,mean])
    value=[255,255,255])
    aimg = b
    #~ 
    #~ anum = "%05d" % j
    #~ cv2.putText(image, anum, (x,y), cv2.FONT_HERSHEY_SIMPLEX, 0.6, (200,200,0), 2)
    #~ cv2.imwrite('tmp/s-'+anum+'-aimg.png', aimg)
    #~ cv2.imwrite('tmp/s-'+anum+'-fimg.png', fimg)
    #~ 
    #~ cv2.imwrite('tmp/context.png', image)
    return [aimg,fimg]

def get_sal(img):
    # initialize
    imgsize = img.shape
    img_width  = imgsize[1]
    img_height = imgsize[0]
    sm = psal.pySaliencyMap(img_width, img_height)
    # computation
    saliency_map = sm.SMGetSM(img)
    binarized_map = sm.SMGetBinarizedSM(img)
    salient_region = sm.SMGetSalientRegion(img)
    # visualize
#    plt.subplot(2,2,1), plt.imshow(img, 'gray')
    plt.subplot(2,2,1), plt.imshow(cv2.cvtColor(img, cv2.COLOR_BGR2RGB))
    plt.title('Input image')
#    cv2.imshow("input",  img)
    plt.subplot(2,2,2), plt.imshow(saliency_map, 'gray')
    plt.title('Saliency map')
#    cv2.imshow("output", map)
    plt.subplot(2,2,3), plt.imshow(binarized_map)
    plt.title('Binarilized saliency map')
#    cv2.imshow("Binarized", binarized_map)
    plt.subplot(2,2,4), plt.imshow(cv2.cvtColor(salient_region, cv2.COLOR_BGR2RGB))
    plt.title('Salient region')
#    cv2.imshow("Segmented", segmented_map)
    #~ 
    #~ plt.show()
    return saliency_map

def analyze_wpage(wpage='j'):#jam default 
    bimg = cv2.imread('img/wpage/'+wpage+'.png')
    bgim = cv2.cvtColor(bimg, cv2.COLOR_BGR2GRAY)
    #~ find product aois
    ret, timg = cv2.threshold(bgim, 245, 255, cv2.THRESH_BINARY)
    eimg = find_edges(timg)
    aoil,image = find_contours(eimg, bimg)
    #~ quick and dirty hack to inject undetected aois
    if wpage == 'y':
        aoil.extend([
        [482,5426,215,428],
        [482,5871,215,428]])
    #~ and pasta
    if wpage == 'p':
        aoil.extend([
        [482,5261,215,399]])
        #~ print(aoil)
    #~ sort by y val, x val
    aoil = np.array(aoil)
    i = np.lexsort((aoil[:,0],aoil[:,1]))
    aoil = np.array(aoil[i])
    #~ get wpage saliency
    simg = get_sal(image)
    #~ visualization image
    timg = cv2.cvtColor(simg, cv2.COLOR_GRAY2RGB)
    #~ iterate over aois
    outl = list()
    for i,aoi in enumerate(aoil):
        anum = "%03d" % (i+1)
        x,y,w,h = aoi
        cv2.rectangle(timg, (x,y), (x+w,y+h), (0,255,0), 2)
        cv2.putText(timg, str(anum), (x+10,y+60), cv2.FONT_HERSHEY_SIMPLEX, 2.0, (0,255,0), 4)
        #~ get local features
        #~ aimg,cimg = get_context(image, aoi)
        #~ saliency analysis
        #~ simg1 = get_sal(aimg)
        #~ simg2 = get_sal(cimg)
        aimg = simg[y:y+h, x:x+w]
        outl.append([anum, aoi, np.mean(aimg), np.sum(aimg)])
        #~ print(anum, aoi, np.mean(aimg), np.sum(aimg))
        if False:
            #~ show result
            #~ cv2.imshow('aimg', aimg)
            cv2.imshow('aimg', simg1)
            #~ cv2.imshow('cimg', simg2)
            cv2.imshow('wpage', resize_frame(image))
            key = cv2.waitKey(0) & 0xff
            if key == 27:#esc
                break
    #~ show results
    #~ cv2.imshow('wpage', resize_frame(timg))
    #~ cv2.waitKey(0)
    #~ convert 0-1 to 0-255 values for writing
    #~ timg = timg*255
    #~ cv2.imwrite('img/saliency/wpage-'+wpage+'.png', timg)
    #~ 
    return [outl,timg]

#~ ##################################################################### sample analyses
def get_smpdb(wpage, p, aoil, simg):
    #~ sample database
    dfa = pd.read_csv('csv/etsmp/'+str(p)+'-'+wpage+'r.txt', sep='\t')
    #~ list to dataframe
    #~ df = pd.DataFrame(np.array(my_list).reshape(3,3), columns = list("abc"))
    #~ apply scroll compensation
    dfa['compy'] = dfa.lpory + dfa.scroll
    #~ find, mark visible aois
    ymax = dfa.scroll.max()+1050
    #~ 
    if False:#visualization
        #~ aoi visible
        for aoi in aoil:
            x,y,w,h = aoi[1]
            aoiv = 1 if y+h/2 < ymax else 0
            if aoiv:
                cv2.rectangle(simg, (x,y), (x+w,y+h), (255,0,0), 5)
        #~ dataframe to list
        smpl = dfa.values
        for s in smpl:
            if not math.isnan(s[1]):
                #~ x,y = map(int, s)
                x,y = [int(s[1]), int(s[5])]
                cv2.circle(simg, (x,y), 4, (0,0,255), 2)
        #~ mouseclick list
        mcl = dfa[dfa['msg'].str.contains("mouseclick")].msg.values
        for mc in mcl:
            x,y = [int(i[2:]) for i in mc.split(' ')[-2:]]
            cv2.circle(simg, (x,y), 20, (200,200,0), 10)
        #~ show results
        cv2.imshow('wpage', resize_frame(simg))
        cv2.waitKey(0)
        simg = simg*255
        cv2.imwrite('img/samples/wpage-'+str(p)+'-'+wpage+'.png', simg)
    #~ 
    return dfa
    
def get_etval(dfa, wpage, p, aoi):
    #~ find, mark visible aois
    ymax = dfa.scroll.max()+1050
    #~ aoi position
    x,y,w,h = aoi[1]
    #~ aoi visible
    aoiv = 1 if y+h/2 < ymax else 0
    #~ samples within aoi
    #~ dfb = dfa.loc[(x <= dfa["lporx"] <= x+h) & (y <= dfa["lpory"] <= y+h), 'lpory']
    dfb = dfa[((dfa["lporx"]>=x) & (dfa["lporx"]<=x+w)) & ((dfa["compy"]>=y) & (dfa["compy"]<=y+h))]
    #~ revisits, non-consequtive frames
    #~ fl = dfa['frame'].unique()
    #~ fl.sort()
    #~ fl = sorted(set(range(fl[0], fl[-1] + 1)).difference(fl))
    tl = dfb.time.values
    tl = [s-f for f,s in zip(tl, tl[1:]) if s-f>1000000]
    #~ print()
    arow = [wpage, p, aoi[0], '|'.join(map(str, aoi[1])), aoi[2], aoi[3], aoiv, dfb.shape[0], len(tl)]
    #~ 
    return arow

#~ ##################################################################### frame analyses
def analyze_etsmp(fnum=92, frame=None):
    cmds = "egrep ^"+str(fnum)+"\s csv/31-porxy-scroll-train-frame.csv"
    #~ cmdl = ['egrep', '']
    process = subprocess.Popen(cmds.split(), stdout=subprocess.PIPE)
    output, error = process.communicate()
    for s in output.split('\n')[:-1]:
        #~ l = s.split('\t')
        if s.split('\t')[2]:
            print(s.split('\t'))

def resize_frame(frame, h=1000):
    #~ h = max height pixels 
    r = float(h) / frame.shape[0]
    dim = (int(frame.shape[1] * r), h)
    #~ 
    rimg = cv2.resize(frame, dim, interpolation=cv2.INTER_AREA)
    #~ 
    #~ print(frame.shape)
    return rimg

def analyze_frame(fnum=0, frame=None, scroll=False):
    #~ if not scroll:
        #~ scroll = 0
    #~ scroll+=1
    
    #~ background web page image
    #~ bimg = bimg.copy()
    #~ bimg = cv2.imread('img/allsylt2.png')
    bimg = cv2.imread('img/wpage-pasta.png')
    bgim = cv2.cvtColor(bimg, cv2.COLOR_BGR2GRAY)
    #~ frame image
    #~ fimg = cv2.imread('tmp/00914.png', 0)
    #~ fimg = cv2.imread('tmp/00765.png')
    fgim = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
    #~ crop: 62, 450
    cgim = fgim[62:1000, 460:1450]

    #~ find frame location on background
    (h,w) = cgim.shape[:2]
    #~ with threshold
    result = cv2.matchTemplate(bgim, cgim, cv2.TM_CCOEFF_NORMED)
    threshold = 0.8
    loc = np.where(result >= threshold)
    if loc[0].any():
        min_val,max_val,min_loc,max_loc = cv2.minMaxLoc(result)
        xy = max_loc
        wh = (xy[0]+w, xy[1]+h)
        print(fnum, scroll)

        #~ draw frame broder on bg image
        cv2.rectangle(bimg, (xy), (wh), (0,255,0), 5)

    #~ resize about factor 0.25
    #~ r = 1000.0 / bimg.shape[0]
    #~ dim = (int(bimg.shape[1] * r), 1000)
    #~ rimg = cv2.resize(bimg, dim, interpolation=cv2.INTER_AREA)
    rimg = resize_frame(bimg)

    return rimg

def analyze_video(posl=[630,690]):
    #~ video positions
    fs,fe = posl
    #~ ts,te = [63000,69000]
    #~ get aoi positions
    #~ ol,oi = analyze_wpage()

    #~ cap = cv2.VideoCapture('vid/31-e2ec8c29-bgscrrec.mkv')
    #~ cap = cv2.VideoCapture('vid/13-e2ec8c29-bgscrrec1.mkv')
    cap = cv2.VideoCapture('/home/sol-nhl/get/cog-shop/09-e2ec8c29-bgscrrec1.mkv')
    #~ cap.set(cv2.CAP_PROP_POS_MSEC, 25000)
    #~ cap.set(cv2.cv.CV_CAP_PROP_POS_FRAMES, fs)
    #~ 
    cap.set(cv2.CAP_PROP_POS_FRAMES, fs)
    #~ cap.set(cv2.CAP_PROP_POS_MSEC, ts)

    #~ fnum = 0
    fcnt = cap.get(cv2.CAP_PROP_FRAME_COUNT)
    fnum = cap.get(cv2.CAP_PROP_POS_FRAMES)
    while(cap.isOpened() and fnum<fe):#fcnt
        #~ print(fnum)
        #~ fnum += 1
        fnum = cap.get(cv2.CAP_PROP_POS_FRAMES)
        #~ print(fnum, fcnt)
        ret, frame = cap.read()
        image = frame.copy()
        
        #~ perform image analysis
        #~ image = analyze_frame(fnum, image)
        
        #~ get et data
        #~ image = analyze_etsmp(fnum, image)
        
        #~ ################### visualize
        key = cv2.waitKey(1) & 0xff
        if not ret:
            break
        if key == ord('p'):
            while True:
                key2 = cv2.waitKey(1) or 0xff
                #~ cv2.imshow('wpage', image)
                cv2.imshow('frame', resize_frame(frame, h=500))
                if key2 == ord('p'):
                    break
        #~ cv2.imshow('wpage', image)
        cv2.imshow('frame', resize_frame(frame, h=500))
        if key == 27:
            #~ print(fnum)
            break

        #~ cv2.imwrite('/tmp/cog/'+str(fnum)+'.png', frame)

    cap.release()
    cv2.destroyAllWindows()

#~ ##################################################################### main
def main():
    pass

if __name__ == '__main__':
    ap = argparse.ArgumentParser()
    ap.add_argument('-a', '--action', help='select action')
    args = vars(ap.parse_args())

    #~ background web page image
    #~ bimg = cv2.imread('img/allsylt2.png')
    #~ bgim = cv2.cvtColor(bimg, cv2.COLOR_BGR2GRAY)

    #~ analyze_frame()
    if args['action'] == 'video':
        for ci in ["240.0|1050.0|2817.0"]:
            flst = map(float, ci.split('|'))
            analyze_video(flst[:-1])
        #~ analyze_video()
    elif args['action'] == 'wpage':
        analyze_wpage()
    elif args['action'] == 'etsmp':
        analyze_etsmp()
    elif args['action'] == 'combo':
        #~ read csv data
        outl = list()
        #~ for c in ['j','c']:#['j','p','c','y']
        for c in ['c','y','p']:
            aoil,simg = analyze_wpage(c)
            for p in [9]:
            #~ for p in range(1,61):
                p = "%02d" % p
                if not os.path.exists('csv/etsmp/'+str(p)+'-'+c+'r.txt'):
                    #~ skip to next without error
                    continue
                timg = simg.copy()
                smpdb = get_smpdb(c, p, aoil, timg)
                for a in aoil:
                    arow = get_etval(smpdb, c, p, a)
                    outl.append(arow)
                print(c,p)
        #~ 
        #~ print(np.array(outl))
        #~ list to dataframe
        head = list([
        'trial_id_task',
        'subject_id',
        'aoi_id_product',
        'aoi_position_xywh',
        'aoi_saliency_mean',
        'aoi_saliency_sum',
        'aoi_visible_bin',
        'et_samples_count',
        'et_revisits_count'])
        dfo = pd.DataFrame(np.array(outl).reshape(np.array(outl).shape), columns=head)
        #~ to csv
        dfo.to_csv('csv/dataset.csv', sep='\t')#, mode='a'
    else:
        exit('please provide a valid argument')




