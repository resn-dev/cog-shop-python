import numpy as np
import cv2
import subprocess
import argparse

#~ methods = [
#~ 'cv2.TM_CCOEFF', 
#~ 'cv2.TM_CCOEFF_NORMED', 
#~ 'cv2.TM_CCORR',
#~ 'cv2.TM_CCORR_NORMED', 
#~ 'cv2.TM_SQDIFF', 
#~ 'cv2.TM_SQDIFF_NORMED']

#~ ##################################################################### wpage analyses
def find_contours(closed, image):
    (cnts, _) = cv2.findContours(closed.copy(), cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
    total = 0
    outl = list()
    # loop over the contours
    for i,c in enumerate(cnts):
        # approximate the contour
        peri = cv2.arcLength(c, True)
        approx = cv2.approxPolyDP(c, 0.02 * peri, True)
        # if approximated contour has four points/vertices, then its a rectangle, and
        # test the length of the contour, threshold when more than half the product box is visible
        if len(approx) == 4 and peri >= 800:
            xy1,xy2 = [b for a in approx[1:3] for b in a]
            #~ filter out objects by position
            if xy1[0] > 450 and xy1[1] > 100:
                #~ print(x1,x2)
                cv2.drawContours(image, [approx], -1, (0, 255, 0), 4)
                #~ save good boxes
                outl.append(approx)
    #~ return image
    return [outl, image]

def find_edges(gray):
    edged = cv2.Canny(gray, 10, 250)
    #~ kernel = cv2.getStructuringElement(cv2.MORPH_RECT, (7, 7))
    kernel = cv2.getStructuringElement(cv2.MORPH_RECT, (5, 5))
    closed = cv2.morphologyEx(edged, cv2.MORPH_CLOSE, kernel)
    #~ 
    #~ return edged
    return closed
    
def analyze_wpage():
    bimg = cv2.imread('img/allsylt2.png')
    bgim = cv2.cvtColor(bimg, cv2.COLOR_BGR2GRAY)
    #~ find product aois
    ret, timg = cv2.threshold(bgim, 245, 255, cv2.THRESH_BINARY)
    eimg = find_edges(timg)
    outl,image = find_contours(eimg, bimg)
    #~ show results
    cv2.imshow('wpage', image)
    cv2.waitKey(0)
    #~ cv2.imwrite('img/aois.png', image)
    #~ 
    return outl

#~ ##################################################################### frame analyses
def analyze_etsmp(fnum=92, frame=None):
    cmds = "egrep ^"+str(fnum)+"\s csv/31-porxy-scroll-train-frame.csv"
    #~ cmdl = ['egrep', '']
    process = subprocess.Popen(cmds.split(), stdout=subprocess.PIPE)
    output, error = process.communicate()
    for s in output.split('\n')[:-1]:
        #~ l = s.split('\t')
        if s.split('\t')[2]:
            print(s.split('\t'))

def resize_frame(frame, h=1000):
    #~ h = max height pixels 
    r = float(h) / frame.shape[0]
    dim = (int(frame.shape[1] * r), h)
    #~ 
    rimg = cv2.resize(frame, dim, interpolation=cv2.INTER_AREA)
    #~ 
    #~ print(frame.shape)
    return rimg

def analyze_frame(fnum=0, frame=None, scroll=False):
    #~ if not scroll:
        #~ scroll = 0
    #~ scroll+=1
    
    #~ background web page image
    #~ bimg = bimg.copy()
    #~ bimg = cv2.imread('img/allsylt2.png')
    bimg = cv2.imread('img/wpage-pasta.png')
    bgim = cv2.cvtColor(bimg, cv2.COLOR_BGR2GRAY)
    #~ frame image
    #~ fimg = cv2.imread('tmp/00914.png', 0)
    #~ fimg = cv2.imread('tmp/00765.png')
    fgim = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
    #~ crop: 62, 450
    cgim = fgim[62:1000, 460:1450]

    #~ find frame location on background
    (h,w) = cgim.shape[:2]
    #~ with threshold
    result = cv2.matchTemplate(bgim, cgim, cv2.TM_CCOEFF_NORMED)
    threshold = 0.8
    loc = np.where(result >= threshold)
    if loc[0].any():
        min_val,max_val,min_loc,max_loc = cv2.minMaxLoc(result)
        xy = max_loc
        wh = (xy[0]+w, xy[1]+h)
        print(fnum, scroll)

        #~ draw frame broder on bg image
        cv2.rectangle(bimg, (xy), (wh), (0,255,0), 5)

    #~ resize about factor 0.25
    #~ r = 1000.0 / bimg.shape[0]
    #~ dim = (int(bimg.shape[1] * r), 1000)
    #~ rimg = cv2.resize(bimg, dim, interpolation=cv2.INTER_AREA)
    rimg = resize_frame(bimg)

    return rimg

def analyze_video(posl=[260,200]):
    #~ video positions
    fs,fe = posl
    #~ ts,te = posl
    #~ get aoi positions
    #~ ol,oi = analyze_wpage()

    #~ cap = cv2.VideoCapture('vid/31-e2ec8c29-bgscrrec.mkv')
    cap = cv2.VideoCapture('vid/13-e2ec8c29-bgscrrec1.mkv')
    #~ cap.set(cv2.CAP_PROP_POS_MSEC, 25000)
    cap.set(cv2.cv.CV_CAP_PROP_POS_FRAMES, fs)
    #~ cap.set(cv2.cv.CV_CAP_PROP_POS_MSEC, ts)

    #~ fnum = 0
    fcnt = cap.get(cv2.cv.CV_CAP_PROP_FRAME_COUNT)
    fnum = cap.get(cv2.cv.CV_CAP_PROP_POS_FRAMES)
    while(cap.isOpened() and fnum<fcnt):
        
        #~ fnum += 1
        fnum = cap.get(cv2.cv.CV_CAP_PROP_POS_FRAMES)
        #~ print(fnum, fcnt)
        ret, frame = cap.read()
        image = frame.copy()
        
        #~ perform image analysis
        image = analyze_frame(fnum, image)
        
        #~ get et data
        #~ image = analyze_etsmp(fnum, image)
        
        #~ ################### visualize
        key = cv2.waitKey(1) & 0xff
        if not ret:
            break
        if key == ord('p'):
            while True:
                key2 = cv2.waitKey(1) or 0xff
                cv2.imshow('wpage', image)
                cv2.imshow('frame', resize_frame(frame, h=500))
                if key2 == ord('p'):
                    break
        cv2.imshow('wpage', image)
        cv2.imshow('frame', resize_frame(frame, h=500))
        if key == 27:
            #~ print(fnum)
            break

    cap.release()
    cv2.destroyAllWindows()

#~ ##################################################################### main
def main():
    pass

if __name__ == '__main__':
    ap = argparse.ArgumentParser()
    ap.add_argument('-a', '--action', help='select action')
    args = vars(ap.parse_args())

    #~ background web page image
    #~ bimg = cv2.imread('img/allsylt2.png')
    #~ bgim = cv2.cvtColor(bimg, cv2.COLOR_BGR2GRAY)

    #~ analyze_frame()
    if args['action'] == 'video':
        for ci in ["240.0|1050.0|2817.0"]:
            flst = map(float, ci.split('|'))
            analyze_video(flst[:-1])
        #~ analyze_video()
    elif args['action'] == 'wpage':
        analyze_wpage()
    elif args['action'] == 'etsmp':
        analyze_etsmp()
    else:
        exit('please provide a valid argument')


#~ cati = ["250.0|1040.0|2817.0","1380.0|1750.0|2817.0","2140.0|2480.0|2817.0"]

#~ p = (250.0, 1040.0, 2817.0)
#~ c = (1380.0, 1750.0, 2817.0)
#~ y = (2140.0, 2480.0, 2817.0)



